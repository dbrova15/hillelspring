package org.example.app.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class NewslitNewsSearchImpl implements GetNews{
    @Override
    public String makeReqUrl(String q) {
        return "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=" + q + "&api-key=VR52GV91lJG2Z7Vm7eFkXkf8wNNdSxZN";
    }

    @Override
    public JsonNode request(String q) throws IOException, InterruptedException {
        String reqUrl = makeReqUrl(q);
        try {
            return ParsReq.parsNewslitNewsSearch(ReqUtil.get(reqUrl));
        } catch (Exception e) {
            e.printStackTrace();
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readTree("{Error: " + e.getMessage() + "}");
        }
    }
}

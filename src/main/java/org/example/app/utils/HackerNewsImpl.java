package org.example.app.utils;

import com.fasterxml.jackson.databind.JsonNode;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

public class HackerNewsImpl implements GetNews {
    @Override
    public String makeReqUrl(String q) {
        return "https://community-hacker-news-v1.p.rapidapi.com/topstories.json?print=pretty";
    }

    public JSONObject getTopStories() throws IOException, InterruptedException {
        JSONObject jsonReq = new JSONObject();

//        List<Integer> listIdStories = (List<Integer>) getTopStoriesId();
//        for (int i = 0; i < listIdStories.size(); i++) {
//            jsonReq.append(getStory(listIdStories.get(i)));
//
//        }
        return null;
    }

    public static JsonNode getStory(int id) throws IOException, InterruptedException {
        String url = "https://hacker-news.firebaseio.com/v0/item/"+ id + ".json?print=pretty";
        return ReqUtil.get(url);
    }

    public static JsonNode getTopStoriesId() throws IOException, InterruptedException {
        JsonNode jsonReq = ReqUtil.get("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty");
        System.out.println(jsonReq);
        System.out.println("==================");
        return jsonReq;
    }

//    public static void main(String[] args) throws IOException, InterruptedException {
//        System.out.println(getTopStoriesId());
//    }
}

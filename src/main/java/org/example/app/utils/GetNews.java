package org.example.app.utils;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

public interface GetNews {
    String url = "";
    String apiKey = "";

    String makeReqUrl(String q);

    default JsonNode request(String q) throws IOException, InterruptedException {
        String reqUrl = makeReqUrl(q);

        return ReqUtil.get(reqUrl);
    }
}

package org.example.app.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ReqUtil {

    public static JsonNode get(String reqUrl) throws IOException, InterruptedException {
        JsonNode obj = null;

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(reqUrl))
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

//        System.out.println(response.body());
        try {
            obj = new ObjectMapper().readTree((response.body()));
        } catch (JSONException err) {
            System.out.println(err.toString());
        }
        return obj;
    }
}

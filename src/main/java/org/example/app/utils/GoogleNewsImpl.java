package org.example.app.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.text.ParseException;

@Setter
@Getter
public class GoogleNewsImpl implements GetNews {
    //   private String url = "https://newsapi.org/v2/everything?q=apple&from=2021-06-22&to=2021-06-22&sortBy=popularity&apiKey=1663a410f4564289bf1596d23885dbdd";
    private static String url = "https://newsapi.org/v2/everything";
    private static String apiKey = "1663a410f4564289bf1596d23885dbdd";

    @Override
    public String makeReqUrl(String q) {
        return url + "?q=" + q + "&sortBy=popularity&apiKey=" + apiKey;
    }

    @Override
    public JsonNode request(String q) throws IOException, InterruptedException {
        String reqUrl = makeReqUrl(q);
        try {
            return ParsReq.parsGoogleNews(ReqUtil.get(reqUrl));
        } catch (Exception e) {
            e.printStackTrace();
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readTree("{Error: " + e.getMessage() + "}");
        }
    }
}

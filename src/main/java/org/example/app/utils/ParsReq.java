package org.example.app.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ParsReq {
    public static JsonNode parsGoogleNews(JsonNode reqString) throws JsonProcessingException, ParseException {
        JsonNode lastText = null;
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        Date lastDateTime = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900");
        JsonNode articles = new ObjectMapper().readTree(reqString.toString()).get("articles");

        for (int i = 0; i < articles.size(); i++) {
            JsonNode o = articles.get(i).get("publishedAt");
            SimpleDateFormat formatter=new SimpleDateFormat(dateFormat);
            Date dateTime = formatter.parse(o.asText().replace("T", " ").replace("Z", ""));
            if (lastDateTime.before(dateTime)) {
                lastDateTime = dateTime;
                lastText = articles.get(i);
            }
        }
        return lastText;
    }

    public static JsonNode parsGuardianNews(JsonNode jsonObject) throws ParseException, JsonProcessingException {
        JsonNode lastText = null;
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        Date lastDateTime = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900");
        JsonNode articles = new ObjectMapper().readTree(jsonObject.toString()).get("response").get("results");

        for (int i = 0; i < articles.size(); i++) {
            JsonNode o = articles.get(i).get("webPublicationDate");
            SimpleDateFormat formatter=new SimpleDateFormat(dateFormat);
            Date dateTime = formatter.parse(o.asText().replace("T", " ").replace("Z", ""));
            if (lastDateTime.before(dateTime)) {
                lastDateTime = dateTime;
                lastText = articles.get(i);
            }
        }
        return lastText;
    }

    public static JsonNode parsNewslitNewsSearch(JsonNode jsonObject) throws ParseException, JsonProcessingException {
        JsonNode lastText = null;
        String dateFormat = "yyyy-MM-dd HH:mm:ss";
        Date lastDateTime = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/1900");
        JsonNode articles = new ObjectMapper().readTree(jsonObject.toString()).get("response").get("docs");

        for (int i = 0; i < articles.size(); i++) {
            JsonNode o = articles.get(i).get("pub_date");
            SimpleDateFormat formatter=new SimpleDateFormat(dateFormat);
            Date dateTime = formatter.parse(o.asText().replace("T", " ").replace("Z", ""));
            if (lastDateTime.before(dateTime)) {
                lastDateTime = dateTime;
                lastText = articles.get(i);
            }
        }
        return lastText;
    }

//    public static void main(String[] args) throws JsonProcessingException, ParseException {
//        String reqString = "{\"q\":{\"name\":\"Dima\",\"surname\":\"Brova\",\"ages\":32,\"hobies\":[\"sport\",\"music\",\"programing\"],\"films\":{\"advengers\":10,\"terminator 2\":10,\"spiderman\":9}}}";
//        String reqString = "{\"status\":\"ok\",\"totalResults\":3083,\"articles\":[{\"source\":{\"id\":\"engadget\",\"name\":\"Engadget\"},\"author\":\"https://www.engadget.com/about/editors/mat-smith\",\"title\":\"The Morning After: Robot dog maker Boston Dynamics has a new owner\",\"description\":\"Today’s headlines: Steven Spielberg will produce movies for Netflix, Samsung drops the curve for its latest gaming monitors, and robot dog maker Boston Dynamics is now owned by Hyundai..\",\"url\":\"https://www.engadget.com/the-morning-after-robot-dog-maker-boston-dynamics-has-a-new-owner-112012290.html\",\"urlToImage\":\"https://s.yimg.com/os/creatr-uploaded-images/2021-06/b3462d30-d266-11eb-bb7e-bcb19f5866d3\",\"publishedAt\":\"2021-06-22T11:20:12Z\",\"content\":\"If you havent already bought a new gaming monitor in the online shopping chaos we like to call Prime Day, Samsung might have something to halt any rash purchases. It unveiled its latest batch of budg… [+3727 chars]\"},{\"source\":{\"id\":\"techcrunch\",\"name\":\"TechCrunch\"},\"author\":\"Steve Gillmor\",\"title\":\"Gillmor Gang: Who Knew\",\"description\":\"The Gillmor Gang recording session is nestled on Friday midday on the East Coast and midmorning out West. Streamed live on Twitter, Facebook Live, and Youtube embed, the show is then edited, sweetened with titles and music, and released on Techcrunch. It’s a …\",\"url\":\"http://techcrunch.com/2021/06/21/gillmor-gang-who-knew/\",\"urlToImage\":\"https://techcrunch.com/wp-content/uploads/2020/07/F06D2685-6169-42C0-AE3B-D2545A1B3534.jpeg?w=711\",\"publishedAt\":\"2021-06-22T05:38:14Z\",\"content\":\"The Gillmor Gang recording session is nestled on Friday midday on the East Coast and midmorning out West. Streamed live on Twitter, Facebook Live, and Youtube embed, the show is then edited, sweetene… [+4855 chars]\"},{\"source\":{\"id\":\"techcrunch\",\"name\":\"TechCrunch\"},\"author\":\"Carly Page\",\"title\":\"Transmit Security raises $543M Series A to kill off the password\",\"description\":\"Transmit Security, a Boston-based startup that’s on a mission to rid the world of passwords, has raised a massive $543 million in Series A funding. The funding round, said to be the largest Series A investment in cybersecurity history and one of the highest v…\",\"url\":\"http://techcrunch.com/2021/06/22/transmit-security-raises-543m-series-a-to-kill-off-the-password/\",\"urlToImage\":\"https://techcrunch.com/wp-content/uploads/2021/06/GettyImages-509815386.jpg?w=600\",\"publishedAt\":\"2021-06-22T10:00:22Z\",\"content\":\"Transmit Security, a Boston-based startup that’s on a mission to rid the world of passwords, has raised a massive $543 million in Series A funding.\\r\\nThe funding round, said to be the largest Series A… [+2124 chars]\"}]}";
//        JsonNode lastText = parsGoogleNews(reqString);
//        System.out.println(lastText);
//    }


}

package org.example.app.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class TheGuardianNewsImpl implements GetNews{
    @Override
    public String makeReqUrl(String q) {
        return  "https://content.guardianapis.com/search?q="+ q + "&api-key=04cf88b9-33ce-47bd-82e3-0aed34641cf4";
    }

    @Override
    public JsonNode request(String q) throws IOException, InterruptedException {
        String reqUrl = makeReqUrl(q);
        try {
            return ParsReq.parsGuardianNews(ReqUtil.get(reqUrl));
        } catch (Exception e) {
            e.printStackTrace();
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readTree("{Error: " + e.getMessage() + "}");
        }
    }
}

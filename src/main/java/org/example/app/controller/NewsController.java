package org.example.app.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.example.app.utils.GoogleNewsImpl;
import org.example.app.utils.NewslitNewsSearchImpl;
import org.example.app.utils.TheGuardianNewsImpl;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("news")
public class NewsController {

    @GetMapping("/test")
    public String test() {
        return "TEST";
    }

    @GetMapping("/google")
    public JsonNode googleNews() throws IOException, InterruptedException {
        GoogleNewsImpl googleNews = new GoogleNewsImpl();
        return googleNews.request("tesla");
    }

    @GetMapping("/guardian")
    public JsonNode guardianNews() throws IOException, InterruptedException {
        TheGuardianNewsImpl theGuardianNewsImpl = new TheGuardianNewsImpl();
        return theGuardianNewsImpl.request("tesla");
    }

    @GetMapping("/newslitNews")
    public JsonNode newslitNewsNews() throws IOException, InterruptedException {
        NewslitNewsSearchImpl newslitNewsSearch = new NewslitNewsSearchImpl();
        return newslitNewsSearch.request("tesla");
    }

}

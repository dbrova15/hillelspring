package org.example.app;

import com.fasterxml.jackson.databind.JsonNode;
import org.example.app.utils.GetNews;
import org.example.app.utils.NewslitNewsSearchImpl;
import org.example.app.utils.TheGuardianNewsImpl;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
        GetNews getNews = ctx.getBean(GetNews.class);
        JsonNode req = getNews.request("tesla");
        System.out.println(req);


//        GoogleNewsImpl googleNews = new GoogleNewsImpl();
//        JSONObject req = googleNews.request("tesla");
//        System.out.println(req);
//
//        System.out.println("\n================================\n");
//
//        NewslitNewsSearchImpl newslitNewsSearch = new NewslitNewsSearchImpl();
//        JsonNode req2 = newslitNewsSearch.request("tesla");
//        System.out.println(req2);
//
//        System.out.println("\n================================\n");
//
//        TheGuardianNewsImpl theGuardianNews = new TheGuardianNewsImpl();
//        JsonNode req3 = theGuardianNews.request("tesla");
//        System.out.println(req3);
    }
}
